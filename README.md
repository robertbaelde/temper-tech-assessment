# Temper tech assessment 

## Project setup
This project concists of two parts. The frontend and the backend. 

## Backend
### Start the backend server
* Cd to the `backend` folder
* run `composer install`
* start the server: `php -S localhost:8000 public/index.php`

### Run unit tests 
* Run `phpunit`, make sure to have the dev dependencies installed

## Frontend 

### Quickstart
* Cd to the frontend folder
* run `yarn install` or `npm install`
* run `yarn run serve` or `npm serve`

### install modules
```
yarn install
```

### Configure frontend
Adjust the `axios.defaults.baseURL` in `main.js` when using another backend location than `http://localhost:8000/`.

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

## Questions
For questions about this repo please contact robert@baelde.nl
