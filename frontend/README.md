# frontend

## Project setup
```
yarn install
```

Adjust the `axios.defaults.baseURL` in `main.js` when using another backend location than `http://localhost:8000/`.

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
