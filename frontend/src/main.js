import Vue from "vue";
import App from "./App.vue";
import HighchartsVue from "highcharts-vue";
import axios from "axios";

Vue.use(HighchartsVue);
Vue.config.productionTip = false;

// Must be put in a config file when project becomes larger.
axios.defaults.baseURL = "http://localhost:8000/";

new Vue({
	render: h => h(App)
}).$mount("#app");
