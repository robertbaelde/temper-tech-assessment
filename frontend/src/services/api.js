import axios from "axios";

export default class Api {
	getUserOnboardingFlowChart() {
		return axios
			.get("api/v1/charts/user-oboarding-flow-chart")
			.then(data => {
				return data.data;
			});
	}
}
