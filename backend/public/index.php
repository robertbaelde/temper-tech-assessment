<?php 
// Enable strict types
declare(strict_types=1);
// autoload composer psr
require __DIR__.'/../vendor/autoload.php';

// enable cors for development
 header("Access-Control-Allow-Origin: *");

// Bootstrap the application 
$app = TemperAssessment\App\App::bootstrap();

// Handle the request
$app->handleRequest($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);