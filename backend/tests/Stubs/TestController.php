<?php
namespace TemperAssessment\Test\Stubs;

use TemperAssessment\Http\Responses\JsonResponse;

class TestController
{
	protected $injected;
	public function __construct($class = null)
	{
	    $this->injected = $class;
	}
	public function handle()
	{
	    return new JsonResponse(['data' => ['foo' => 'bar']]);
	}

	public function getInjected()
	{
	    return $this->injected;
	}
}