<?php 

namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\App\DependencyInjector;
use TemperAssessment\Test\Stubs\FooInterface;
use TemperAssessment\Test\Stubs\TestFoo;

class DependencyInjectionContainerTest extends TestCase
{
    /** @test */
    function it_stores_the_classes_to_map()
    {
    	$dependencyInjector = new DependencyInjector;
    	$dependencyInjector->map(FooInterface::class, new TestFoo);

    	$class = $dependencyInjector->resolve(FooInterface::class);

    	$this->assertInstanceOf(TestFoo::class, $class);
    }

    /** @test */
    function a_exception_is_thrown_when_class_is_not_bind()
    {
    	$this->expectException(\Exception::class);
    	
        $dependencyInjector = new DependencyInjector;

    	$class = $dependencyInjector->resolve(FooInterface::class);
    }
}