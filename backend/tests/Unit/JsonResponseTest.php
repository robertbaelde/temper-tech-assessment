<?php
namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\Http\Responses\JsonResponse;

class JsonResponseTest extends TestCase
{
	/**
	 * Run in seperate process to avoid header aleardy set error
     * @runInSeparateProcess
     @test 
     */
    function it_renders_the_data_as_json()
    {
    	$this->expectOutputString('{"foo":"bar"}');
    	// header content type:application/json should be set. But is assumed out of scope to test right now
    	$response = new JsonResponse(['foo' => 'bar']);
    	$response->render();
    }
}