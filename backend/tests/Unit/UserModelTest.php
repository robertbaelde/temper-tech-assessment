<?php
namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\Models\User;

class UserModelTest extends TestCase
{
    /** @test */
    function it_can_return_the_weeknumber_in_which_the_user_was_created()
    {
        $user = new User(['created_at' => '2018-08-19']); // 2018-08-19 is week 33

        $this->assertEquals(33, $user->getWeekNumberRegistered());
    }
}