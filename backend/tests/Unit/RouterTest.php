<?php
namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\Http\Responses\JsonResponse;
use TemperAssessment\Http\Responses\MethodNotAllowedResponse;
use TemperAssessment\Http\Responses\NotFoundResponse;
use TemperAssessment\Http\Router;
use TemperAssessment\Test\Stubs\TestFoo;

class RouterTest extends TestCase
{
	/** @test */
	function it_throws_a_404_response_when_the_requested_route_does_not_exists()
	{
		$router = new Router;
		$response = $router->handleRequest('get', '/not-existing');

		$this->assertInstanceOf(NotFoundResponse::class, $response);
	}

	/** @test */
	function it_calls_the_configured_class_when_a_url_exists()
	{
		$router = new Router;
		$router->get('/exists', 'TemperAssessment\Test\Stubs\TestController@handle');
		$response = $router->handleRequest('get', '/exists');

		$this->assertInstanceOf(JsonResponse::class, $response);
	}

	/** @test */
	function it_throws_a_405_method_not_allowed_when_the_wrong_method_is_used()
	{
	    $router = new Router;
	    $router->get('/exists', 'TemperAssessment\Test\Stubs\TestController@handle');
		$response = $router->handleRequest('post', '/exists');

		$this->assertInstanceOf(MethodNotAllowedResponse::class, $response);
	}

	/** @test */
	function classes_injected_to_the_router_contructor_are_injected_to_the_controllers()
	{
	    $router = new Router(new JsonResponse);
		$router->get('/exists', 'TemperAssessment\Test\Stubs\TestController@getInjected');
		$response = $router->handleRequest('get', '/exists');

		$this->assertInstanceOf(JsonResponse::class, $response);
	}


}