<?php 

namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\App\DependencyInjector;
use TemperAssessment\Http\Controllers\UserOnboardingFlowChartController;
use TemperAssessment\Http\Responses\JsonResponse;
use TemperAssessment\Models\User;
use TemperAssessment\Repositories\UserRepositoryInterface;
use TemperAssessment\Test\MockUserRepository;

class UserOnboardingFlowChartControllerTest extends TestCase
{
    /** @test */
    function it_returns_the_mapped_data()
    {
    	$dependencyInjector = new DependencyInjector;
    	$dependencyInjector->map(UserRepositoryInterface::class, new MockUserRepository([
    		33 => [
    			new User(['id' => 1, 'created_at' => '2018-08-19', 'onboarding_percentage' => 40]),
    			new User(['id' => 2, 'created_at' => '2018-08-19', 'onboarding_percentage' => 20]),
                new User(['id' => 3, 'created_at' => '2018-08-19', 'onboarding_percentage' => 100]),
    			new User(['id' => 4, 'created_at' => '2018-08-19', 'onboarding_percentage' => 100]),
    		],
    	]));

    	$controller = new UserOnboardingFlowChartController($dependencyInjector);
    	$response = $controller->get();

    	$this->assertInstanceOf(JsonResponse::class, $response);
    	$this->assertEquals([
    		'data' => [
    			'cohorts' => [
    				33 => [
    					'data' => [
                            ["step 1", 100],
                            ["step 2", 100],
                            ["step 3", 75],
                            ["step 4", 50],
                            ["step 5", 50],
                            ["step 6", 50],
                            ["step 7", 50],
                            ["step 8", 50]
                        ],
                        'cohort_size' => 4
    				]
    			]
    		]
    	],$response->data);
    }
}