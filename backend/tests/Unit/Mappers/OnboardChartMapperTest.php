<?php
namespace TemperAssessment\Test\Unit\Mappers;

use PHPUnit\Framework\TestCase;
use TemperAssessment\Mappers\MapCohortToOnboardingChart;
use TemperAssessment\Models\User;

class OnboardChartMappersTest extends TestCase
{
    /** @test */
    function it_returns_the_mapped_data()
    {
    	$mapped_data = (new MapCohortToOnboardingChart([
			new User(['id' => 1, 'onboarding_percentage' => 0]),
			new User(['id' => 2, 'onboarding_percentage' => 20]),
			new User(['id' => 3, 'onboarding_percentage' => 40]),
			new User(['id' => 4, 'onboarding_percentage' => 50]),
			new User(['id' => 5, 'onboarding_percentage' => 70]),
			new User(['id' => 6, 'onboarding_percentage' => 90]),
			new User(['id' => 7, 'onboarding_percentage' => 99]),
			new User(['id' => 8, 'onboarding_percentage' => 100]),
    	]))->map();
    	// user percentage decreases 12,5% per step in this cohort
    	$this->assertEquals([
    		'data' => [
    			["step 1", 100],
				["step 2",  87.5],
				["step 3",  75.0],
				["step 4",  62.5],
				["step 5",  50.0],
				["step 6",  37.5],  
				["step 7",  25.0],
				["step 8",  12.5]
			], 'cohort_size' => 8
		], $mapped_data);
    }
}