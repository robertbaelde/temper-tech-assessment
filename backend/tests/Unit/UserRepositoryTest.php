<?php
namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\Models\User;
use TemperAssessment\Repositories\CsvUserRepository;

class UserRepositoryTest extends TestCase
{
    /** @test */
    function the_csv_repository_returns_all_customers_from_a_csv_file()
    {
        $users = (new CsvUserRepository(__DIR__."/../Stubs/userCsvStub.csv"))->all();

        $this->assertCount(10, $users);
        $this->assertInstanceOf(User::class, $users[0]);
        $this->assertEquals(1, $users[0]->id);
        $this->assertEquals('2018-08-19', $users[0]->created_at);
        $this->assertEquals(40, $users[0]->onboarding_percentage);
    }

    /** @test */
    function the_csv_repository_can_return_users_grouped_by_weekly_cohort()
    {
        $users_grouped = (new CsvUserRepository(__DIR__."/../Stubs/userCsvStub.csv"))->groupedByCohort();

        $this->assertCount(3, $users_grouped);
        $this->assertCount(3, $users_grouped[33]);
        $this->assertCount(4, $users_grouped[34]);
        $this->assertCount(3, $users_grouped[35]);
    }
}