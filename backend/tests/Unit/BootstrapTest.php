<?php
namespace TemperAssessment\Test\Unit;

use PHPUnit\Framework\TestCase;
use TemperAssessment\App\App;
use TemperAssessment\Http\Router;
use TemperAssessment\Models\User;

class BootstrapTest extends TestCase
{
    /** @test */
    function it_registers_the_dependency_injector_and_the_router_when_bootstrapping()
    {
    	$app = App::bootstrap();
    	
    	$this->assertInstanceOf(App::class, $app);
    	$this->assertInstanceOf(Router::class, $app->router);
    }
}