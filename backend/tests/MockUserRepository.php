<?php
namespace TemperAssessment\Test;

use TemperAssessment\Repositories\UserRepositoryInterface;

class MockUserRepository implements UserRepositoryInterface
{
	protected $array = [];

	public function __construct(array $array)
	{
	    $this->array = $array;
	}

	public function all(): array
	{
		return $this->array;
	}

	public function groupedByCohort(): array
	{
	   return $this->array;
	}
}