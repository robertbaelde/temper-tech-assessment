<?php
namespace TemperAssessment\Mappers;

use TemperAssessment\Models\User;

class MapCohortToOnboardingChart
{
	protected $cohort;

	protected $steps = [
		['title' => 'step 1', 'completion' => 0],
		['title' => 'step 2', 'completion' => 20],
		['title' => 'step 3', 'completion' => 40],
		['title' => 'step 4', 'completion' => 50],
		['title' => 'step 5', 'completion' => 70],
		['title' => 'step 6', 'completion' => 90],
		['title' => 'step 7', 'completion' => 99],
		['title' => 'step 8', 'completion' => 100]
	];

	/**
	 * Creates the mapper
	 * @param array $cohort Array of users
	 */
	public function __construct(array $cohort)
	{
		$this->cohort = $cohort;
	}

	/**
	 * Map the data
	 * @return array returns the fromatted array
	 */
	public function map(): array
	{
		$data = [];
		$cohort_size = count($this->cohort);

		foreach ($this->steps as $step) {
			$data[] = [$step['title'], $this->toPercentage($this->countCompletedMinPercentage($step['completion']), $cohort_size)];
		}

		return [
			'data' => $data, 
			'cohort_size' => $cohort_size
		];
	}

	/**
	 * counts the number of users that completed a specific step
	 * @param  float  $percent percentage to have at least
	 * @return int  Returns the number of users
	 */
	protected function countCompletedMinPercentage(float $percent): int
	{
		return count(array_filter($this->cohort, function(User $user) use ($percent) {
			return $user->onboarding_percentage >= $percent;
		}));
	}

	/**
	 * Calculates the percentage
	 * @param  float  $number number of users
	 * @param  int    $total  total number of users
	 * @return float Percentage of users
	 */
	protected function toPercentage(int $number, int $total): float
	{
		return ($number/$total)*100;
	}
}