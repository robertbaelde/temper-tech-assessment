<?php
namespace TemperAssessment\Http\Controllers;

use TemperAssessment\Http\Responses\JsonResponse;
use TemperAssessment\Http\Responses\ResponseInterface;
use TemperAssessment\Mappers\MapCohortToOnboardingChart;
use TemperAssessment\Repositories\UserRepositoryInterface;

class UserOnboardingFlowChartController extends BaseController
{
	public function get(): ResponseInterface
	{
	    $cohorts = ($this->injected->resolve(UserRepositoryInterface::class))->groupedByCohort();

	    $data = [];
	    foreach ($cohorts as $week => $cohort) {
	    	$data[$week] = (new MapCohortToOnboardingChart($cohort))->map();
	    }
	    
	    return new JsonResponse([
	    	'data' => [
	    		'cohorts' => $data
	    	]
	    ]);
	}
}