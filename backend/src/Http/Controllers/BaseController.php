<?php
namespace TemperAssessment\Http\Controllers;

use TemperAssessment\Http\Responses\JsonResponse;

class BaseController
{
	protected $injected;
	public function __construct($class = null)
	{
	    $this->injected = $class;
	}
}