<?php
namespace TemperAssessment\Http\Responses;

class NotFoundResponse extends JsonResponse
{
	public function __construct()
	{
	    parent::__construct(['msg' => 'not found']);
	}

	private function setHeaders()
	{
		header('HTTP/1.0 404 Not Found');
	}
	
	public function render()
	{
		$this->setHeaders();
		parent::render();
	}
}