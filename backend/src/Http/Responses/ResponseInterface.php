<?php
namespace TemperAssessment\Http\Responses;

interface ResponseInterface
{
	public function render();
}