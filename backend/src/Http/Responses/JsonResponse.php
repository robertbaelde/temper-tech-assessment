<?php
namespace TemperAssessment\Http\Responses;

class JsonResponse implements ResponseInterface
{
	public $data;
	/**
	 * Constructs the response
	 * @param array|null $data Data to be returned
	 */
	public function __construct(array $data = null)
	{
	    $this->data = $data;
	}

	/**
	 * Set content-type header to application/json
	 * @return void
	 */
	private function setHeaders()
	{
		header('Content-type:application/json;charset=utf-8');
	}
	
	/**
	 * Render the response
	 * @return void
	 */
	public function render()
	{
		$this->setHeaders();
		echo json_encode($this->data);
	}
}