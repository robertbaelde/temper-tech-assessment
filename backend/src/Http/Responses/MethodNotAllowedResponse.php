<?php
namespace TemperAssessment\Http\Responses;

class MethodNotAllowedResponse extends JsonResponse
{

	public function __construct()
	{
	    parent::__construct(['msg' => 'not allowed']);
	}

	private function setHeaders()
	{
		header('HTTP/1.0 405 Method Not Allowed');
	}
	public function render()
	{
		$this->setHeaders();
		parent::render();
	}
}