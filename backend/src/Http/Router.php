<?php
namespace TemperAssessment\Http;

use TemperAssessment\Http\Responses\MethodNotAllowedResponse;
use TemperAssessment\Http\Responses\NotFoundResponse;
use TemperAssessment\Http\Responses\ResponseInterface;

class Router
{
	protected $injected;

	protected $routes = [];

	/**
	 * Construct the router
	 * @param class $injected Class that can be injected to all routers
	 */
	public function __construct($injected = null)
	{
	    $this->injected = $injected;
	}

	/**
	 * Register a get request
	 * @param  string $uri    Uri to map controllre to
	 * @param  string $action {Name of the controller}@{method}
	 * @return void
	 */
	public function get(string $uri, string $action)
	{
		if (!$this->uriConfigured($uri)) {
			$this->routes[$uri] = [];
		}
	    $this->routes[$uri]['get'] = $action;
	}

	/**
	 * Find the route by uri and handles it
	 * @param  string $method Http method used
	 * @param  string $uri   uri requested
	 * @return ResponseInterface Returns a response
	 */
	public function handleRequest(string $method, string $uri): ResponseInterface
	{
		$method = strtolower($method);
		if(!$this->uriConfigured($uri))
		{
	    	return new NotFoundResponse();
		}

		if(!$this->methodConfigured($uri, $method))
		{
	    	return new MethodNotAllowedResponse();
		}
		
		return $this->callAction($this->routes[$uri][$method]);
	}

	/**
	 * Check if uri is configured
	 * @param  string $uri
	 * @return bool returns true when uri is found
	 */
	private function uriConfigured(string $uri): bool
	{
		return array_key_exists($uri, $this->routes);
	}

	/**
	 * Check if Http method is configured
	 * @param  string $uri   
	 * @param  string $method Http method
	 * @return bool returns true when route is found 
	 */
	private function methodConfigured(string $uri, string $method): bool
	{
		return array_key_exists($method, $this->routes[$uri]);
	}

	/**
	 * Instanciate the right router, and calls the configured action
	 * @param  string $action Controller@Action to be called
	 * @return ResponseInterface returns response interface returned from the controller
	 */
	private function callAction(string $action): ResponseInterface
	{
		list($controller, $method) = explode("@", $action);
		if(!class_exists($controller)){
			die("throw exception that controller doesnt exist");
		}
		$func = array(new $controller($this->injected), $method);
		return $func();
	}
}