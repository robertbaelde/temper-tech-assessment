<?php
namespace TemperAssessment\Models;

class User
{
	protected $fields = ['id', 'created_at', 'onboarding_percentage'];

	/**
	 * Creates a new user instance
	 * @param array $data array of fields to be filled
	 */
	public function __construct(array $data)
	{
		foreach ($this->fields as $field) {
			if(array_key_exists($field, $data)){
				$this->{$field} = $data[$field];
			}
		}
	}

	/**
	 * Get week number registered
	 * @return int Week number
	 */
	public function getWeekNumberRegistered(): int
	{
		return (new \DateTime($this->created_at))->format("W");
	}
}