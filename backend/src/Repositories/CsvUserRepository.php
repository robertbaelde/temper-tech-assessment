<?php
namespace TemperAssessment\Repositories;

use TemperAssessment\Models\User;

class CsvUserRepository implements UserRepositoryInterface
{
	protected $file_path;
	protected $users = [];

	/**
	 * Creates new User Repository
	 * @param string $file_path Path to the csv file to be used
	 */
	public function __construct(string $file_path)
	{
		$this->file_path = $file_path;
		$this->loadCsv();
	}

	/**
	 * Load and parse the csv file into memory
	 * @return void
	 */
	private function loadCsv()
	{
		if (($handle = fopen($this->file_path, "r")) !== FALSE) {
			$first_row = true;
		  	while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
		  		if($first_row){
		  			$first_row = false;
		  			continue;
		  		}
		  		$this->users[] = new User([
		  			'id' => $data[0],
		  			'created_at' => $data[1],
		  			'onboarding_percentage' => $data[2],
		  		]);
		  	}
		  	fclose($handle);
		}
	}

	/**
	 * Get array of all Users
	 * @return array Array of users
	 */
	public function all(): array
	{
		return $this->users;
	}

	/**
	 * Get all usses grouped by cohort
	 * @return array Array of cohorts
	 */
	public function groupedByCohort(): array
	{
	    $grouped = [];
	    foreach ($this->users as $user) {
	    	$grouped[$user->getWeekNumberRegistered()][] = $user;
	    }
	    return $grouped;
	}
}