<?php
namespace TemperAssessment\Repositories;

interface UserRepositoryInterface
{
	public function all(): array;
	public function groupedByCohort(): array;
}