<?php
namespace TemperAssessment\App;

use TemperAssessment\App\DependencyInjector;
use TemperAssessment\Http\Router;
use TemperAssessment\Repositories\CsvUserRepository;
use TemperAssessment\Repositories\UserRepositoryInterface;

class App
{
	private $dependency_injector;
	private $default_csv_path = __DIR__."/../../storage/export.csv";
	public $router;

	/**
	 * Bootstrap the application
	 * @return App returns a new instance of this class
	 */
	public static function bootstrap(): App
	{
		$self = new Static;
		$self->createDependencyInjector();
		$self->mapDependencies();
		$self->startRouter();
		$self->mapRoutes();
		return $self;
	}

	/**
	 * Maps the request to the router and renders the response
	 * @param  string $method Http method used
	 * @param  string $uri    Uri called
	 * @return void 
	 */
	public function handleRequest(string $method, string $uri)
	{
	    $response = $this->router->handleRequest($method, $uri);
	    $response->render();
	}

	/**
	 * Creates the dependency injector
	 * @return void
	 */
	private function createDependencyInjector()
	{
		$this->dependency_injector = new DependencyInjector;
	}

	/**
	 * Map all dependencies. This could be in a seperate config file 
	 * when the application grows.
	 * @return void
	 */
	private function mapDependencies()
	{
		$this->dependency_injector->map(UserRepositoryInterface::class, new CsvUserRepository($this->default_csv_path));
	}

	/**
	 * Create the router
	 * @return void
	 */
	private function startRouter()
	{
		$this->router = new Router($this->dependency_injector);
	}

	/**
	 * Map all urls to the specific controllers. 
	 * This could be in a seperate config file 
	 * when the application grows.
	 * @return void
	 */
	private function mapRoutes()
	{
		$this->router->get('/api/v1/charts/user-oboarding-flow-chart', 'TemperAssessment\Http\Controllers\UserOnboardingFlowChartController@get');
	}
}