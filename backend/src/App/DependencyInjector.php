<?php
namespace TemperAssessment\App;

class DependencyInjector
{
	protected $bindings = [];

	/**
	 * Map a binding to a class
	 * @param  string $dependency 	name of the dependency
	 * @param  class $binding 		Instance of the class to be used
	 * @return void
	 */
	public function map(string $dependency, $binding)
	{
	 	$this->bindings[$dependency] = $binding;
	}

	/**
	 * Resolve a dependency
	 * @param  string $dependency  Mame of the dependency
	 * @return class  the bounded class
	 */
	public function resolve(string $dependency)
	{
	    if(!array_key_exists($dependency, $this->bindings)){
	    	throw new \Exception("DI: class {$dependency} not found");
	    }
	    return $this->bindings[$dependency];
	}
}